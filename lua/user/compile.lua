local P = {}

local function simple_file_exists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end

-- A compile_command is a table which specifies how a particular
-- filetype/project is "compiled".
-- The `checks` table contains a list of check names to check runner functions,
-- each of which return a "pass"/"fail" string depending on the check.
-- If all of the test pass, then the `cmd` function is executed.
-- Here's the structure of a compile_command.
--
-- local compile_command = {
--   checks = { [check_name]: [check_fn => "pass"/"fail"] },
--   cmd    = [compile_fn],
-- }

local make = {
  checks = {
    ["DirectoryHasMakefile"] = function ()
      if simple_file_exists("Makefile") then
        return "pass"
      end
      return "fail"
    end,
  },
  cmd = function() vim.cmd [[ :!make ]] end,
}

local xelatex = {
  checks = {
    ["FiletypeIsTex"] = function ()
      local ft = vim.bo.filetype
      if (ft == "tex") or (ft == "latex") then
        return "pass"
      end
      return "fail"
    end
  },
  cmd = function() vim.cmd [[ :!latexmk -pdf % ]] end,
}

local compile_commands = {
  ["tex"] = xelatex,
  ["c"]   = make,
  ["cpp"] = make,
  ["c++"] = make,
  ["asm"] = make,
}

-- This is the function which actually examines a buffer and determines the
-- commands to run (bind this to a key-combination).
function P.compile_buffer()
  local fn = "compile_buffer"

  local ft = vim.bo.filetype
  local cc = compile_commands[ft]
  if cc == nil then
    print(string.format("[%s] no compile commands for filetype %q", fn, ft))
    return
  end
  -- make sure any checks pass
  if cc.checks ~= nil then
    for check_name, check_run in pairs(cc.checks) do
      local result = check_run()
      if result ~= "pass" then
        print(string.format("[%s] check %q failed", fn, check_name))
        return
      end
    end
  end
  if cc ~= nil then cc.cmd() end
end

return P
